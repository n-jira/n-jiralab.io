+++
audios = []
descricao = "### Ananse tecendo teias na diáspora: \n\n#### uma narrativa de resistência e luta das herdeiras e dos herdeiros de Ananse\n\n![](imagens/71wmqmzy09l.jpg)\n\nAtravés da afro-diáspora, milhares de africanas e africanos vieram de maneira compulsória para as Américas. Mas não vieram sozinhos. Trouxeram suas tradições, crenças, valores civilizatórios, línguas, religiões, histórias, conhecimentos e saberes específicos, entre outros. Submetidos à dura realidade da escravidão, essas africanas e africanos e seus descendentes elaboraram inúmeras formas de resistência contrárias ao sistema escravista e a favor da manutenção de sua cultura: a fuga, as sabotagens, a formação de quilombos e a contação de histórias foram apenas algumas delas. Ao cruzarem o Atlântico vieram acompanhados, também, do mito da aranha e divindade Anansi, que muito nos ensina sobre o processo de contação de histórias presente nas tradições culturais africanas, bem como do universo da ressignificação dessas culturas como forma de resistência. A metáfora de Anansi contribui para a compreensão de uma visão de mundo africana, calcada no paradigma afrocêntrico de conhecimento, e tem se espalhado de modo que suas teias teçam histórias, memórias e tradições orais como forma de educar. Compreendendo quilombos como territórios negros onde ainda resistem muitos aspectos africanos através da prática da contação de histórias e o reconhecimento de todo um conjunto de saberes, conhecimentos, identidades, organizações sociais negras e africanas predominantemente negligenciadas por uma abordagem eurocêntrica de conhecimento, mas que ainda resistem no interior de comunidades quilombolas, refletimos aqui sobre uma escola brasileira mais plural, alimentada por valores da cosmovisão africana ainda presentes nos quilombos e por um currículo escolar que valorize saberes e conhecimentos não hierarquizados como prevê a Lei 10.639/03."
galeria = []
links_relacionados = []
tags = ["Zélia Amador", "GEZA"]
title = "Ananse tecendo teias na diáspora_ Zélia Amador De Deus"
[capa]
descricao = ""
imagem = ""
[[pdfs]]
ano = 2019
arquivo = "imagens/anansetecendoteiasnadiaspora_zeliaamadordedeus_secult2019.pdf"
autoria = "Zélia Amador de Deus"
titulo = "Ananse tecendo teias na diáspora: uma narrativa de resistência e luta das herdeiras e dos herdeiros de Ananse"

+++
