+++
audios = []
descricao = "Aldeia Sawré Muybu, 28 de novembro de 2014\n\n![](imagens/iii-carta-da-autodemarcacao-do-territorio-daje-kapap-eypi-30112014-docx-1.png)\n\n“Quando nós passamos onde porcos passaram, eu vi, eu tive uma visão deles passando. Eu tenho 30 anos. Quando eu era criança minha mãe me contou a história dos porcos. É por isso que devemos defender nossa mãe terra. As pessoas devem respeitar também. Todas as pessoas devem respeitar porque a história está viva ainda, estamos aqui, somos nós”, Orlando BorÔ Munduruku, aldeia Waro Apompu do Alto Tapajós.\n\nHoje, pela primeira vez durante a autodemarcação, chegamos ao local sagrado Daje Kapap Eypi, onde os porcos atravessaram levando o filho do Guerreiro Karasakaybu. Sentimos algo muito poderoso que envolveu todo nosso corpo.\n\nOutra emoção forte que sentimos hoje foi ver nossa terra toda devastada pelo garimpo bem perto de onde os porcos passaram. Nosso santuário sagrado está sendo violado, destruído 50 pc’s (retroescavadeiras) em terra e 5 dragas no rio. Para cada escavadeira, 5 pobres homens, em um trabalho de semiescravidão, explorados de manhã até a noite por 4 donos estrangeiros.\n\nPirmeiro o governo federal acabou Sete Quedas, no Teles Pires, que foi destruído pela hidrelétrica, matando o espírito da cachoeira. E agora, com seu desrespeito em não publicar o nosso relatório, acaba também com Daje Kapap Eypi.\n\nSentimos o chamado. Nosso guerreiro, nosso Deus, nos chamou. Karosakaybu diz que devemos defender nosso território e nossa vida do grande Daydo, o traidor, que tem nome: O governo Brasileiro e seus aliados que tentam de todas as formas nos acabar.\n\nNós estamos lutando pela nossa demarcação há muitos anos, sempre que a gente vai pra Brasília a FUNAI inventa mentiras e promessas pra nos acalmar. Sabemos que a Funai faz isso para ganhar o tempo para construção da hidrelétrica do Tapajós, agora nós cansamos de esperar.\n\nSem chorar ou transformando as lágrimas em coragem, em Assembléia tomamos a seguinte decisão: A FUNAI tem três dias para publicar o nosso relatório e dar continuidade à demarcação, homologação e desintrusão da nossa terra.\n\nCaso não sejamos atendidos, vamos dar continuidade ao trabalho da autodemarcação até o final. Por enquanto só estamos avisando os invasores que eles devem sair do nosso território, mas, se a Funai não fizer o que tem que ser feito, ou seja, publicar o nosso relatório e demarcar nossa terra, a mesma, com sua omissão, estará provocando um conflito com proporções inimagináveis entre Munduruku e invasores, que já é anunciado há muito tempo, com todas as denúncias de ameaças que estamos sofrendo."
galeria = ["imagens/iii-carta-da-autodemarcacao-do-territorio-daje-kapap-eypi-30112014-docx-1.png"]
tags = ["Ipereg Ayu Munduruku", "Munduruku", "Carta"]
title = "III Carta da autodemarcação do Território DAJE KAPAP EYPI"
[capa]
descricao = ""
imagem = ""
[[links_relacionados]]
link = "https://movimentoiperegayu.wordpress.com/2014/11/30/iii-carta-da-autodemarcacao-do-territorio-daje-kapap-eypi/"
titulo = ""
[[pdfs]]
ano = 2014
arquivo = "imagens/iii-carta-da-autodemarcacao-do-territorio-daje-kapap-eypi-30112014-docx.pdf"
autoria = "Ipereg Ayu Munduruku"
titulo = "III Carta da autodemarcação do Território DAJE KAPAP EYPI"

+++
