+++
audios = []
descricao = "**SOBRE A PELE, O RIO: VIAGENS, FLUXOS CONTÍNUOS, AMBIENTES, EXPERIÊNCIAS INTUITIVAS, O CORPO OS ATLAS DE R(EX)SISTÊNCIA** \n\n###### Sobre la piel, el rio: viajes, flujos contínuos, ambientes, experiências intuitivas, el cuerpo e los atlas de r(ex)sistencias \n\n**Cláudia Leão**\n\n**Luana Peixoto** \n\nUniversidade Federal do Pará \n\nFaculdade de Artes Visuais – Grupo de Pesquisa Lab AMPE \n\n**RESUMO**\n\n Este artigo pretende pensar as viagens, como processos artísticos dentro de um circuito de vivências intensas entre paisagens, passagens e relações com os outros às que chamo de fluxos contínuos. Assim esse procedimento vem constituir as tessituras entre corpo, mapas e desenhos de r(ex)sistências, nas tentativas de compreender o sentido de pertença, relações de amor, saudade, experiências intuitivas; arte, cultura e política na região Amazônica são alguns dos intentos deste trabalho. As viagens a que me remeto, são as realizadas de barco pelos rios Amazonas e Xingu. A base teórica onde está fundamentada esta pesquisa está no conceito de paisagem, ambiente e corpo entrelaçamentos a partir do filósofo japonês Tetsuro Watsuji e do geográfo Eidorfe Moreira; de vivências e experiencia do antropólogo Ryuta Imafuku, e o sentido de viagem para o poeta japonês, Matsuo Bashô.\n\n \n\nPALAVRAS-CHAVE: ARTE; VIAGENS, FLUXOS CONTÍNUOS; INTUIÇÃO; PAISAGENS, RIOS DAAMAZÔNIA"
galeria = []
links_relacionados = []
tags = ["LABampe", "Artigo Científico "]
title = "SOBRE A PELE, O RIO"
[capa]
descricao = ""
imagem = ""
[[pdfs]]
ano = 2015
arquivo = "imagens/artigo_cl-lp_dlfluxos-continuos-ambientes_-docx.pdf"
autoria = "Cláudia Leão, Luana Peixoto"
titulo = "SOBRE A PELE, O RIO: VIAGENS, FLUXOS CONTÍNUOS, AMBIENTES, EXPERIÊNCIAS INTUITIVAS, O CORPO OS ATLAS DE R(EX)SISTÊNCIA"

+++
