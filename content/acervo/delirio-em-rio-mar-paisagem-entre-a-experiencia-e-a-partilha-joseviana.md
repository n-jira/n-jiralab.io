+++
audios = []
descricao = "### DELÍRIO EM RIO MAR \n\n#### paisagem entre a experiência e a partilha\n\n**José de Almeida Viana Junior**\n\nUNIVERSIDADE FEDERAL DO PARÁ\n\n Instituto de Ciências da Arte \n\nPrograma de Pós Graduação em Artes \n\nDissertação de Mestrado Dissertação apresentada à Universidade Federal do Pará, como parte das exigências do Programa de Pós\u0002Graduação em Artes, linha de pesquisa II - Arte em Interfaces Epistêmicas, para a obtenção do título de Mestre. \n\npesquisa **José de Almeida Viana Junior**\n\n orientação **Dra. Ana Cláudia do Amaral Leão**\n\nBelém, Pará, Brasil, 2019"
galeria = []
links_relacionados = []
tags = ["José Viana", "Dissertação "]
title = "Delirio em Rio Mar - paisagem entre a experiencia e a partilha [joseviana]"
[capa]
descricao = ""
imagem = ""
[[pdfs]]
ano = 2019
arquivo = "imagens/delirio-em-rio-mar-paisagem-entre-a-experiencia-e-a-partilha-joseviana.pdf"
autoria = "José de Almeida Viana Junior"
titulo = "Delirio em Rio Mar - paisagem entre a experiencia e a partilha [joseviana]"

+++
