---
type: sobre
title: 'O que é o aninga.org'
---


A aninga é uma planta amazônica aquática, fluida com os rios, cria ilhas e torna mais resistentes as beiras, perenemente expostas à natural erosão que causam a incidência dos fluxos constantes.

Tem um caule comprido, as folhas, que ficam na ponta às raízes, são em formato de coração com as ramificações atrás que se assemelham à espinha de um peixe, dá uma flor branca, grande, que se destaca no verde forte das folhas. O seu comprimento transpassa universos diferentes, o fundo lodoso do rio, o meio e a superfície da água, e o ar de fora, cria uma linha entre eles. É prosaica e bonita como é prosaica e bonita a vida que corre com a água.

A aninga é uma planta que faz da Amazônia (essa palavra estrangeira, nome de registro, e hoje nosso) internamente reconhecível, quem mora na beira do rio sabe o que é, sabe o que faz e para que serve, que bicho que come e os que nela gostam de ficar.

Suas sementes são carregadas pelas águas e nascem em muitas margens, como nascem as casas palafitas de famílias ribeirinhas perto da água, imersas no silêncio denso que carrega o corpo milenar dos rios.

Partindo das experiências vividas nas regiões da Transamazônica e Xingu e na região do/no rio Tapajós, desde 2011, no período da implantação da UHE de Belo Monte, momento de intensa movimentação política no Estado do Pará, a Rede Aninga propõe o desenvolvimento de uma plataforma digital que organiza, gerencia e disponibiliza acervos de conteúdos artevistas, políticos, e informativos da região amazônica, notadamente focada em produções que se sustentam em modelos irreverentes, autonomistas, por meio da cartografia social e de falas de grupos e/ou indivídu_e_s, atentando para o armazenamento e cuidado que este tipo de produção requer e que geralmente encontram-se fora do eixo institucional das artes, da cultura e a política. A finalidade é tornar visível este tipo de produção, contextualizá-lo, para assim, disponibilizá-lo para consulta pública.

A Rede Aninga também foi pensada como uma forma de contribuir para facilitar o fluxo de informações e ações artístico-culturais na região amazônica paraense, assim como, também é uma estratégia de segurança e prevenção das diversas violações aos direitos garantidos constitucionalmente, por meio da sua divulgação, circulação e disponibilização de estudos realizados sobre e com povos e comunidades tradicionais amazônidas, levando em concideração a dificuldade de conectividade à distância de tais comunidades, a sua vulnerabilidade e a amplitude da região amazônica.

O desenvolvimento da rede Aninga conta ainda com a parceria com coletivos artísticos, associações e grupos políticos de povos e comunidades tradicionais amazônicos.

A Rede Aninga propõe o desenvolvimento de uma plataforma digital que organiza, gerencia e disponibiliza acervos de conteúdos artísticos políticos da região amazônica, notadamente focada em produções que se sustentam em modelos irreverentes autonomistas, e geralmente encontra-se fora do eixo institucional das artes e cultura. A rede visiona a produção de bibliotecas digitais dentro desta plataforma, com conteúdos selecionados a partir da cartografia social de grupos e/ou indivíduos, atentando para o armazenamento e cuidado que este tipo de produção requer. A finalidade é tornar visível este tipo de produção, contextualizá-lo, para assim disponibilizá-lo para consulta pública.

Entendemos a cartografia como uma ferramenta metodológica que nos permite localizar o tempo histórico(cronologia que fez sentido para nós e para nossas histórias) ressaltando o contexto político e aspectos culturais que permeiam estas produções. O processo de organização desses acervos a partir de métodos cartográficos também nos possibilitam pensar coletivamente, considerando a subjetividade destas produções …
